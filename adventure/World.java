/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure;

/**
 *
 * @author John
 */

import java.util.ArrayList;


public class World { 

   
    public static ArrayList<Location> locArr;
    private ArrayList locations;
    public int[][] gameWorld;
    public static World myWorld;
    public static BreadCrumbTrail trail;

    public World(ArrayList locations){
        
        gameWorld = new int[][] {
            
            {0, 1, 0, 0, 0, 0, 0, 0}, //lower decks
            {1, 0, 1, 1, 0, 1, 0, 0}, //alleyway
            {0, 1, 0, 1, 0, 0, 0, 0}, //bread room
            {0, 1, 0, 0, 1, 0, 0, 0}, //storage
            {0, 0, 0, 0, 0, 1, 0, 0}, //bridge
            {0, 1, 0, 0, 1, 0, 0, 0}, //poop deck
            {0, 0, 0, 0, 0, 1, 0, 0}, //crow's nest
            {0, 1, 0, 0, 0, 0, 0, 0}, //captain's quarters
           
        };
        
                
    }
    
    public int getIndex(Location loc){
        
        for(int i = 0; i < locArr.size(); i++){
            if(loc.equals(locArr.get(i))){
                return i;
            }
        }
        
        return -1;
        
    }
    
    
    
    public static void initialize(){
        
        locArr = new ArrayList();
        
        

        Item map = new Item("map", "A map of the ship.");
        Item katana = new Item("katana", "A sword belonging to an ancient samurai, only usable by ninjas.");
        Item blunderbuss = new Item("blunderbuss", "A close quarters firearm only usable by pirates.");
        Item key = new Item("captain's key", "The key to the Captain's Quarters.");
        Item hook = new Item("hook", "A simple hook");
        
        Location loc1 = new Location("lowerdecks", "The Lower Decks of the Ship."); //index 0
        Location loc2 = new Location("alleyway", ""); //index 1
        Location loc3 = new Location("breadroom", ""); //2
        Location loc4 = new Location("storage", ""); //3
        Location loc5 = new Location("bridge", ""); //4
        Location loc6 = new Location("poopdeck", ""); //5
        Location loc7 = new Location("crow'snest", ""); //6
        Location loc8 = new Location("captain'squarters", ""); //7

        
        locArr.add(loc1);
        locArr.add(loc2);
        locArr.add(loc3);
        locArr.add(loc4);
        locArr.add(loc5);
        locArr.add(loc6);
        locArr.add(loc7);
        locArr.add(loc8);
       
        loc1.addItem(hook);
        loc4.addItem(katana);
        loc4.addItem(blunderbuss);
        loc5.addItem(map);
        loc7.addItem(key);
               
        trail = new BreadCrumbTrail();
        trail.add(0, locArr.get(0));
        
        myWorld = new World(locArr);
        
        
    }
}
