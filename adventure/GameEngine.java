
package adventure;

/**
 *
 * @author John
 */

import java.util.Scanner;
import java.util.ArrayList;
import javax.swing.JLabel;

public class GameEngine {
    
    private GameState state;
    public Player player1;
    private ArrayList<Item> itemArr;
    private String name;
            
    public GameEngine(){
        
        this.state = new GameState();    
    
    }

    public static void main(String[] args) {
        
        GameEngine eng = new GameEngine();
        eng.runEngine();
        
    }
    
    public void runEngine(){
        World.initialize();
        introduction();
        this.gameLoop();
        credits();
        
    }
    
    
    public static void display(String text) {
        
        //GUI.output.setText("<html><br>" + GUI.output.getText() + "<html><br>" + text);
        if(GUI.stage == 0){
            GUI.output.setText(GUI.output.getText() + text + "\n");
        }
        else if(GUI.stage == 1){
            
            GUI.output.setText(GUI.output.getText() + text + "\n");
            
        }
        if(GUI.stage == 2){
            GUI.output.setText(GUI.output.getText() + text + "\n");
        }
        
    }
    
    
    public void introduction(){       
        name = GUI.input.getText().toLowerCase();
                               
    }
    
    public void intro2(){
        String job = "";
        while(!job.equals("pirate")&&!job.equals("ninja")){
          display("Hello " + name + ". Are you a pirate or a ninja?");
          job = GUI.input.getText().toLowerCase();
        }
        GUI.output.setText("");
        player1 = new Player(name, job, World.myWorld.locArr.get(0));
        if(job.equals("pirate")){
            display("Ye be a dog of the sea, matey? A fine decision!");
        }
        if(job.equals("ninja")){
            display("You have decided to follow the path of the ninja. A wise choice.");
        }
        display("Now let's begin your journey...\n");
        display("Your arms burn as you row your oarboat further and further. "
                + "It's been almost a day now and you can finally see your destination. "
                + "Out in the middle of the Ocean, a single ship "
                + "bearing the flag of the jolly rodger beckons to you. "
                + "under the cover of the night, you row up as close as possible to the ship,"
                + " and enter through a small circular window in the lower decks.");
    }
    
    public static void credits(){
        display("Thanks for playing!");
    }
    
    public String[] parseInput(){
        
        
        String cmd = GUI.input.getText().toLowerCase();
        cmd = cmd.toLowerCase();
        String[] cmdarr = cmd.split(" ");
        return cmdarr;

    }
    
    public void actionHandler(String action, String object){
        switch(action){
            case "q": credits(); break;
            case "m": player1.move(object); break;
            case "t": player1.take(object); break;
            case "d": player1.drop(object); break;
            case "i" : display(player1.toString()); break;
            case "b" : player1.backtrack(object); break;
            default: display("Command not recognized.");

        }
    }
    
    public void  gameLoop(){
        
                
        String[] cmd;
        
            cmd = parseInput();
            String action = cmd[0];
            String object = "";
            if(cmd.length > 1){
                object = cmd[1];
            }
            actionHandler(action, object);
            int moves = World.trail.size() - 1;
            player1.getItems();
            GUI.stats.setText("<html><center>STATS</center><br>Moves: <html>" + (World.trail.size()-1)
            + "<html><br>Current Location: <html>" + player1.currentLoc.getName()
            );
    }
 
    
    public static String help(){
        return "The available commands are as follows: \n q: quit the game "
                + "\n m: move to a new location \n t: take an item and add it to your inventory"
                + "\n d: drop an item from your inventory \n i: check what items you currently have in your inventory. "
                + "\n b: go back a specified number of steps." ;
    }
}
    
