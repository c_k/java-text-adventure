/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure;

/**
 *
 * @author John
 */
import java.util.ArrayList;

public class Location {
    
    private String name;
    private String description;
    ArrayList<Item> items;
    
    public Location(String name, String description){
        
        this.name = name;
        this.description = description;
        items = new ArrayList();
        
    }
    
    public  String getName(){
        
        return name;
        
    }
    
    public String getDescription(){
        
        return description;
        
    }
    
    public void addItem(Item item){
        
        items.add(item);
        
    }
    
    public void removeItem(Item item){
        items.remove(item);
    }
    

    public boolean has(String item){
        
        if(items.size() < 1){
            return false;
        }
        
        for(int i = 0; i < this.items.size(); i++){
            if(this.items.get(i).getName().equals(item)){
                return true;
            }
        }
       
        return false;
   
    }
    
    public ArrayList<Item> getItems(){
        return this.items;
    }
   
    
    /*
    Location loc1 = new Location("Lower Decks", "The Lower Decks of the Ship.", null);
    Location loc2 = new Location("Alleyway", "", null);
    Location loc3 = new Location("Bread Room", "", null);
    Location loc4 = new Location("Storage", "", null);
    Location loc5 = new Location("Bridge", "", null);
    Location loc6 = new Location("Poop Deck", "", null);
    Location loc7 = new Location("Crow's Nest", "", null);
    Location loc8 = new Location("Captain's Quarters", "", null);   
    */
    
}
