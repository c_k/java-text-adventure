
package adventure;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.TextArea;

public class GUI {

    private static JFrame frame;
    public static JTextField input;
    public static TextArea output;
    private JPanel panel;
    private GameEngine engine = new GameEngine();
    public static int stage = 0;
    public static JLabel itemsTitle;
    public static JLabel statsTitle;
    public static JLabel items;
    public static JLabel stats;
    
    public JPanel buildPanel(){
        
        panel = new JPanel();
        panel.add(new JLabel("Enter a command:"));
        panel.add(this.input);
        return panel;
    }
    
    public GUI(){
        
        this.frame = new JFrame("Pirates vs. Ninjas");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.getContentPane().setPreferredSize(new Dimension(400, 300));
        this.frame.getContentPane().add(this.buildGUI());
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
     
    }
   
    
    
    
    private JPanel buildGUI(){
        
        output = new TextArea("test", 1, 1, 1);
        output.setEditable(false);

        JLabel title = new JLabel("<html><body><h1>Pirates vs. Ninjas</h1></body></html>");
        title.setHorizontalAlignment(JLabel.CENTER);
        JPanel inputsPanel = this.buildInputControls();
        JPanel panel = new JPanel();
        
        items = new JLabel("<html><center>ITEMS</center><html>");
        stats = new JLabel("<html><center>STATS</center><br>Moves: <html>" + "<html><br>Current Location: <html>");
                
        panel.setLayout(new BorderLayout());
        panel.add(title, BorderLayout.PAGE_START);
        panel.add(output, BorderLayout.CENTER);
        panel.add(items, BorderLayout.EAST);
        panel.add(stats, BorderLayout.WEST);
        panel.add(inputsPanel, BorderLayout.PAGE_END);
        
        return panel;
        
    }
    
    private JPanel buildInputControls() {
        // used in the ActionListener inner classes
        final GameEngine eng = new GameEngine();

        JButton helpBtn = new JButton("Help");
        helpBtn.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(frame, GameEngine.help());
            }
        });

        this.input = new JTextField(8);
        this.input.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(stage == 0){
                    engine.introduction();
                    GUI.output.setText("Are you a pirate or a ninja?");
                    stage++;
                }
                else if (stage == 1){
                    engine.intro2();
                    stage++;
                }
                else if(stage == 2){
                    engine.gameLoop();
                }
               ((JTextField)e.getSource()).setText(null);
            }
        });

        JPanel panel = new JPanel();
        panel.add(new JLabel("Enter a command:"));
        panel.add(this.input);
        panel.add(helpBtn);
        return panel;
    }  
    
    
    public void welcome(){
        
        JOptionPane.showMessageDialog(this.frame, "Hello, welcome to my game!");
        GUI.output.setText("What is your name?");
        
    }
    
    protected void showGoodbye() {
    	
        final GameEngine eng = new GameEngine();
        this.frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
            	JOptionPane.showMessageDialog(frame, "Thanks for playing my game!");
            }
        });
    }
    
    public static void main(String[] args) {
        World.initialize();
        GameEngine engine = new GameEngine();
        GUI gui = new GUI();
        gui.welcome();
        frame.setVisible(true);
        gui.showGoodbye();
    }
        
}
